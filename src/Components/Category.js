import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

class Category extends Component {
  render() {
    return (
      <View
        style={{
          height: 139,
          width: 224,
          marginLeft: 20,
          borderRadius: 20,
          borderWidth: 0.5,
          borderColor: '#dddddd',
        }}>
        <View style={{flex: 1}}>
          <Image
            source={this.props.imageUri}
            style={{flex: 1, width: null, height: null, resizeMode: 'cover'}}
          />
        </View>
      </View>
    );
  }
}
export default Category;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
