import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={{height: 74, flexDirection: 'row'}}>
        <Image
          source={require('../assets/catalogue/leftPanelIcon.png')}
          style={{flex: 1, resizeMode: 'contain', height: 23, marginTop: 16}}
        />
        <View style={{height: 74, flex: 2}}>
          <Text
            style={{
              fontSize: 20,
              color: '#000000',
              marginTop: 10,
              textAlign: 'left',
            }}>
            All Products
          </Text>
          <Text style={{fontSize: 12, color: '#006cff'}}>
            Flora Green Apts, Gachibowli
          </Text>
        </View>

        <Image
          source={require('../assets/catalogue/search.png')}
          style={{height: 23, marginTop: 16, flex: 1, resizeMode: 'contain'}}
        />
      </View>
    );
  }
}

export default Header;
