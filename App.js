import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Category} from './src/Components/Category';
import Header from './src/Components/Header';

class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Header></Header>
        </View>
        <View>
          <Text>Must Need</Text>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Image
              source={require('./src/assets/catalogue/home.jpeg')}
              style={{
                height: 139,
                width: 224,
                marginLeft: 20,
                borderRadius: 20,
                borderWidth: 0.5,
                borderColor: '#dddddd',
              }}
            />
          </ScrollView>
        </View>
        {/* <View style={styles.categoryContainer}>
          <Text style={styles.h1}>Categories</Text>
        </View>
        <View style={styles.categoryContainer}>
          <Image source={require('./src/assets/catalogue/milk.png')} />
          <Text>Milk & Milk Products</Text>
          <TouchableOpacity style={styles.imageButton}>
            <Image
              style={styles.image}
              source={require('./src/assets/catalogue/navigationButton.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.separationLine} /> */}
      </View>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  categoryContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#ffff',
    alignContent: 'space-between',
  },
  h1: {
    color: '#000000',
    fontSize: 16,
    fontWeight: 'bold',
  },
  image: {
    alignSelf: 'stretch',
  },
  separationLine: {
    borderColor: '#d4d4d4',
    borderWidth: 1,
    height: 1,
    width: 330,
  },
  imageButton: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 0.5,
    borderColor: '#fff',
    height: 40,
    width: 220,
    borderRadius: 5,
    margin: 5,
  },
});
